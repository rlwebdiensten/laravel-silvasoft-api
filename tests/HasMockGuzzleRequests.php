<?php
/**
 * Created by PhpStorm.
 * User: veerle@daktadeo.be
 * Date: 15/10/2018
 * Time: 15:41
 * (c) DaktaDeo
 */

namespace DaktaDeo\Silvasoft\Test;
use DaktaDeo\Silvasoft\Client;
use DaktaDeo\Silvasoft\Exceptions\IsRequired;
use DaktaDeo\Silvasoft\Relation;
use GuzzleHttp\Client as GuzzleClient;
//use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Silvasoft;

trait HasMockGuzzleRequests {
	/**
	 * Create a Mock POST guzzle request & response, that checks endpoint & parameters
	 *
	 * @param $expectedResponse
	 * @param $expectedEndpoint
	 * @param $expectedParams
	 *
	 * @return \PHPUnit_Framework_MockObject_MockObject
	 */
	private function mock_post_guzzle_request( $expectedResponse, $expectedEndpoint, $expectedParams ) {
		$mockResponse = $this->getMockBuilder( ResponseInterface::class )
		                     ->getMock();
		if ( $expectedResponse ) {
			$mockResponse->expects( $this->once() )
			             ->method( 'getBody' )
			             ->willReturn( $expectedResponse );
		}
		$mockGuzzle = $this->getMockBuilder( GuzzleClient::class )
		                   ->setMethods( [ "post" ] )
		                   ->getMock();
		$mockGuzzle->expects( $this->once() )
		           ->method( "post" )
		           ->with( $expectedEndpoint, $expectedParams )
		           ->willReturn( $mockResponse );
		
		return $mockGuzzle;
	}
	
	/**
	 * Create a Mock POST guzzle request & response, that checks endpoint & parameters
	 *
	 * @param $expectedResponse
	 * @param $expectedEndpoint
	 * @param $expectedParams
	 *
	 * @return \PHPUnit_Framework_MockObject_MockObject
	 */
	private function mock_put_guzzle_request( $expectedResponse, $expectedEndpoint, $expectedParams ) {
		$mockResponse = $this->getMockBuilder( ResponseInterface::class )
		                     ->getMock();
		if ( $expectedResponse ) {
			$mockResponse->expects( $this->once() )
			             ->method( 'getBody' )
			             ->willReturn( $expectedResponse );
		}
		$mockGuzzle = $this->getMockBuilder( GuzzleClient::class )
		                   ->setMethods( [ "put" ] )
		                   ->getMock();
		$mockGuzzle->expects( $this->once() )
		           ->method( "put" )
		           ->with( $expectedEndpoint, $expectedParams )
		           ->willReturn( $mockResponse );
		
		return $mockGuzzle;
	}
	
	/**
	 * Create a Mock GET guzzle request & response, that checks endpoint & parameters
	 *
	 * @param $expectedResponse
	 * @param $expectedEndpoint
	 * @param $expectedParams
	 *
	 * @return \PHPUnit_Framework_MockObject_MockObject
	 */
	private function mock_get_guzzle_request( $expectedResponse, $expectedEndpoint, $expectedParams ) {
		$mockResponse = $this->getMockBuilder( ResponseInterface::class )
		                     ->getMock();
		if ( $expectedResponse ) {
			$mockResponse->expects( $this->once() )
			             ->method( 'getBody' )
			             ->willReturn( $expectedResponse );
		}
		$mockGuzzle = $this->getMockBuilder( GuzzleClient::class )
		                   ->setMethods( [ "get" ] )
		                   ->getMock();
		$mockGuzzle->expects( $this->once() )
		           ->method( "get" )
		           ->with( $expectedEndpoint, [ "query" => $expectedParams ] )
		           ->willReturn( $mockResponse );
		
		return $mockGuzzle;
	}
	
	protected static function getMethod( $name ) {
		$class  = new \ReflectionClass( 'DaktaDeo\Silvasoft\Client' );
		$method = $class->getMethod( $name );
		$method->setAccessible( true );
		
		return $method;
	}
	
}