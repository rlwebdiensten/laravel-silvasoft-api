<?php

namespace DaktaDeo\Silvasoft\Test;

use DaktaDeo\Silvasoft\Client;
use DaktaDeo\Silvasoft\Exceptions\IsRequired;
use DaktaDeo\Silvasoft\Relation;
use GuzzleHttp\Client as GuzzleClient;
//use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Silvasoft;

class ClientTest extends TestCase {
	use HasMockGuzzleRequests;
	
	protected $test_key = "b7d343Bnhd436f3ec3bd3504582";
	protected $test_username = "john@doe.nl";
	
	/** @test */
	public function it_can_be_instantiated() {
		$client = new Client( $this->test_key, $this->test_username );
		$this->assertInstanceOf( Client::class, $client );
	}
	
	/** @test */
	public function it_can_be_called_with_facade(){
		$this->assertSame(Silvasoft::multiply(4, 4), 16);
	}
	

	
}