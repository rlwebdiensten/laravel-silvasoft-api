<?php
namespace DaktaDeo\Silvasoft\Test;

use DaktaDeo\Silvasoft\Client;
use DaktaDeo\Silvasoft\Exceptions\IsRequired;
use DaktaDeo\Silvasoft\Product;
use GuzzleHttp\Client as GuzzleClient;
//use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Silvasoft;

class ProductsTest extends TestCase {
	use HasMockGuzzleRequests;
	
	protected $test_key = "b7d343Bnhd436f3ec3bd3504582";
	protected $test_username = "john@doe.nl";
	
	/** @test */
	public function it_can_get_all_products() {
		$response   = array (
			0 =>
				array (
					'Category' => 'Electronics',
					'EAN' => '123456789120',
					'Description' => 'Product description',
					'StockQty' => 0,
					'Price' => 110,
					'ArticleNumber' => '1204',
					'PurchasePrice' => 101.9500000000000028421709430404007434844970703125,
					'VATPercentage' => 19,
					'SupplierArticleNumber' => NULL,
					'Name' => 'BENQ Monitor 24 inch',
					'Unit' => 'Piece',
				),
			1 =>
				array (
					'Category' => 'Electronics',
					'EAN' => '123456789130',
					'Description' => 'Product description',
					'StockQty' => 100,
					'Price' => 18.925999999999998379962562466971576213836669921875,
					'ArticleNumber' => 'Product description',
					'PurchasePrice' => 4,
					'VATPercentage' => 0,
					'SupplierArticleNumber' => 'ART-E-1234',
					'Name' => 'USB-Cable',
					'Unit' => 'Piece',
				)
		);
		$endpoint   = 'https://mijn.silvasoft.nl/rest/listproducts/';
		$parameters = [ "limit" => 100, "relationtype" => "All" ];
		
		$mockGuzzle = $this->mock_get_guzzle_request( json_encode( $response ), $endpoint, $parameters );
		$client     = new Client( $this->test_key, $this->test_username, $mockGuzzle );
		$items      = $client->listProducts( $parameters );
		$this->assertCount( 2, $items );
	}
	
	/** @test */
	public function when_posting_a_new_product_fields_are_required(){
		$this->expectException(IsRequired::class);
		
		$product = new Product();
		$product->Description = "Product description";
		
		$mockGuzzle = $this->getMockBuilder( GuzzleClient::class )
		                   ->setMethods( [ "post" ] )
		                   ->getMock();
		
		$client     = new Client( $this->test_key, $this->test_username, $mockGuzzle );
		$client->addProduct($product);
	}
	
	/** @test */
	public function it_can_add_a_new_product(){
		$endpoint   = 'https://mijn.silvasoft.nl/rest/addproduct/';
		$response   = [array (
			'ArticleNumber' => '1',
			'CategoryName' => 'Clothes',
			'CategoryCreateIfMissing' => true,
			'EAN' => '123123123',
			'NewName' => 'Product created with API',
			'NewSalePrice' => 15.949999999999999289457264239899814128875732421875,
			'NewStockQty' => 124,
			'NewVATPercentage' => 21,
		)];
		
		$data = Product::hydrate($response);
		$data->flatten();
		$product = $data->first();
		$parameters = ["json" => $product->toArray()];
		
		$mockGuzzle = $this->mock_post_guzzle_request( json_encode( $response ), $endpoint, $parameters );
		$client     = new Client( $this->test_key, $this->test_username, $mockGuzzle );
		
		$items = $client->addProduct($product);
		
		$this->assertCount( 1, $items );
		$this->assertSame($product->toArray(), $items[0]);
	}
	
	/** @test */
	public function when_posting_an_update_relation_fields_are_required(){
		$this->expectException(IsRequired::class);
		
		$product = new Product();
		$product->Description = "Product description";
		
		$mockGuzzle = $this->getMockBuilder( GuzzleClient::class )
		                   ->setMethods( [ "put" ] )
		                   ->getMock();
		
		$client     = new Client( $this->test_key, $this->test_username, $mockGuzzle );
		$client->updateProduct($product);
	}
	
	/** @test */
	public function it_can_update_a_product(){
		$endpoint   = 'https://mijn.silvasoft.nl/rest/updateproduct/';
		$response   =  [array (
			'ArticleNumber' => '1',
			'CategoryName' => 'Clothes',
			'CategoryCreateIfMissing' => true,
			'EAN' => '123123123',
			'NewName' => 'Product created with API',
			'NewSalePrice' => 15.949999999999999289457264239899814128875732421875,
			'NewStockQty' => 124,
			'NewVATPercentage' => 21,
		)];
		
		$data = Product::hydrate($response);
		$data->flatten();
		$product = $data->first();
		
		$parameters = ["json" => $product->toArray()];
		
		$mockGuzzle = $this->mock_put_guzzle_request( json_encode( $response ), $endpoint, $parameters );
		$client     = new Client( $this->test_key, $this->test_username, $mockGuzzle );
		
		$items = $client->updateProduct($product);
		
		$this->assertCount( 1, $items );
		$this->assertSame($product->toArray(), $items[0]);
	}
	
	
}