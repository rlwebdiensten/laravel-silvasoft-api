<?php
return [
	/*
	 * The secret API key for Silvasoft
	 */
	'api_key' => env('SILVASOFT_API_KEY', "b7d343Bnhd436f3ec3bd3504582"),
	/*
	 * Your Silvasoft username
	 */
	'username' => env('SILVASOFT_USERNAME', "john@doe.nl"),
	
	/*
	 * Your Silvasoft API rate limit per hour
	 */
	'api_request_limit' => 15,

];