<?php
namespace DaktaDeo\Silvasoft\Exceptions;

use Exception;
use Psr\Http\Message\ResponseInterface;

class BadRequest extends Exception
{
	/**
	 * The Silvasoft error code supplied in the response.
	 *
	 * @var string|null
	 */
	public $silvasoftCode;
	public function __construct(ResponseInterface $response)
	{
		$body = json_decode($response->getBody(), true);
		$this->silvasoftCode = $body['errorCode'];
		parent::__construct($body['errorMessage']);
	}
}