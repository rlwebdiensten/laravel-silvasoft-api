<?php

namespace DaktaDeo\Silvasoft;

use DaktaDeo\Silvasoft\Exceptions\BadRequest;
use DaktaDeo\Silvasoft\Exceptions\IsRequired;
use Exception;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\RequestOptions;

trait HasEndPoints {
	
	/**
	 * Create a GET endpoint request. When using the magic method we can use our mock guzzle thing in the tests.
	 *
	 * @param string $endpoint
	 * @param array|null $parameters
	 *
	 * @return array
	 * @throws Exception
	 */
	public function getEndpointRequest( string $endpoint, array $parameters = null ): array {
		try {
			$response = $this->client->get( "https://mijn.silvasoft.nl/rest/{$endpoint}", [ "query" => $parameters ] );
		} catch ( ClientException $exception ) {
			throw $this->determineException( $exception );
		}
		$response = json_decode( $response->getBody(), true );
		
		return $response ?? [];
	}
	
	/**
	 * Create a POST endpoint request. When using the magic method we can use our mock guzzle thing in the tests.
	 *
	 * @param string $endpoint
	 * @param array|null $parameters
	 *
	 * @return array
	 * @throws Exception
	 */
	public function postEndpointRequest( string $endpoint, array $parameters = null ): array {
		try {
			$response = $this->client->post( "https://mijn.silvasoft.nl/rest/{$endpoint}", [ RequestOptions::JSON => $parameters ] );
		} catch ( ClientException $exception ) {
			throw $this->determineException( $exception );
		}
		$response = json_decode( $response->getBody(), true );
		
		return $response ?? [];
	}
	
	/**
	 * Create a PUT endpoint request. When using the magic method we can use our mock guzzle thing in the tests.
	 *
	 * @param string $endpoint
	 * @param array|null $parameters
	 *
	 * @return array
	 * @throws Exception
	 */
	public function putEndpointRequest( string $endpoint, array $parameters = null ): array {
		try {
			$response = $this->client->put( "https://mijn.silvasoft.nl/rest/{$endpoint}", [ RequestOptions::JSON => $parameters ] );
		} catch ( ClientException $exception ) {
			throw $this->determineException( $exception );
		}
		$response = json_decode( $response->getBody(), true );
		
		return $response ?? [];
	}
	
	/**
	 * Checks for the code 400, 409 and throws a bad request error
	 *
	 * @param ClientException $exception
	 *
	 * @return Exception
	 */
	protected function determineException( ClientException $exception ): Exception {
		if ( in_array( $exception->getResponse()->getStatusCode(), [ 400, 409 ] ) ) {
			return new BadRequest( $exception->getResponse() );
		}
		
		return $exception;
	}
}