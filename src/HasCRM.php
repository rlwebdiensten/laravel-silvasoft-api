<?php

namespace DaktaDeo\Silvasoft;
use DaktaDeo\Silvasoft\Exceptions\BadRequest;
use DaktaDeo\Silvasoft\Exceptions\IsRequired;
use Exception;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\RequestOptions;

trait HasCRM {
	/**
	 * This GET method allows you to retrieve a list of relations including contact and address details from your administration
	 *
	 * @param array $parameters
	 *
	 * @return Collection of Relations
	 * @throws Exception
	 */
	public function listRelations( Array $parameters ) {
		$arr = $this->getEndpointRequest( 'listrelations/', $parameters );
		$lst = Relation::hydrate( $arr );
		
		return $lst->flatten();
	}
	
	/**
	 * From the Silvasoft docs:
	 *
	 * This POST method allows you to create a new business relation including contact and address details.
	 *
	 * Important!
	 * It is very important to understand how your request will be interpreted since this is a specific process
	 * and the outcome of your request can be unexpected.
	 *
	 * General
	 * Name is required. All parameters (=fields) are CaSeSENsitive (see Silvasoft docs)
	 * Also, the parameters customerNumber and supplierNumber may not be 0. Sending 0 as number will be interpreted as an empty value.
	 *
	 * Parameters
	 * Two parameters in this request needs some explanation: OnExistingRelationNumber and OnExistingRelationName.
	 * We will explain both parameters below.
	 *
	 * OnExistingRelationName This field determines what should happen when the relation you POST already exist,
	 * based on the relation name. The default behaviour is that we will return the already existing relation.
	 * However, if you want the creation of the relation to be forced you can add this parameter in your POST.
	 * When set to CONTINUE we will continue creating the relation, resulting in you having multiple relations
	 * with exactly the same name.
	 * OnExistingRelationNumber This field determines how we should behave when the relation you POST already exist,
	 * based on the customer or supplier number. The default behaviour is that we will return the already existing
	 * relation. However, if you want the creation of the relation to be forced you can add this parameter in your
	 * POST. When set to CONTINUE we will continue creating the relation, but with a new, unique number.
	 * When set to ABORT (default), we will abort the creation and return the already existing relation instead.
	 * Please note that is is not required to post a relation or suppliernumber. If you leave both numbers empty,
	 * we will automatically generate a number for you and return the newly created relation with the automatic defined number.
	 *
	 * @param Relation $relation
	 *
	 * @return array
	 * @throws IsRequired
	 */
	public function addBusinessRelation( Relation $relation ) {
		if ( blank( $relation->Name ) ) {
			throw new IsRequired( "relation-name" );
		}
		$options = $relation->toArray();
		
		return $this->postEndpointRequest( 'addbusinessrelation/', $options );
	}
	
	/**
	 * From the Silvasoft docs:
	 *
	 * This POST method allows you to create a new private relation (consumer) including contact and address details.
	 *
	 * Important!
	 * It is very important to understand how your request will be interpreted since this is a specific process and
	 * the outcome of your request can be unexpected.
	 *
	 * General
	 * All parameters (=fields) are CaSeSENsitive (see Silvasoft docs)
	 * Relation_Contact is required, as are FirstName and LastName. The rest of the parameters are not mandatory.
	 * The response of this request will be the details of the relation. The response format can be found in the
	 * ‘ListRelations’ endpoint response description.
	 *
	 * Also, the parameters customerNumber may not be 0. Sending 0 as number will be interpreted as an empty value for this parameter.
	 *
	 * Parameters
	 * Two parameters in this request needs some explanation: OnExistingRelationNumber and OnExistingRelationName.
	 * We will explain both parameters below.
	 *
	 * OnExistingRelationName This field determines what should happen when the relation you POST already exist, based on the relation name. A match occurs when the relations first- and lastname match. The default behaviour is that we will return the already existing relation. However, if you want the creation of the relation to be forced you can add this parameter in your POST. When set to CONTINUE we will continue creating a new relation, which will result in multiple consumers with the same name existing in your administration.
	 * OnExistingRelationNumber This field determines how we should behave when the relation you POST already exist, based on the customer number. The default behaviour is that we will return the already existing relation. However, if you want the creation of the relation to be forced you can add this parameter in your POST. When set to CONTINUE we will continue creating a new relation, but with a differtent, unique number. When set to ABORT (default), we will abort the creation and return the already existing relation instead.
	 * Please note that is is not required to post a relation number. If you leave it empty, we will automatically generate a number for you and return the newly created relation with the automatic defined number.
	 *
	 * @param Relation $relation
	 *
	 * @return array
	 * @throws IsRequired
	 */
	public function addPrivateRelation(Relation $relation){
		if ( blank( $relation->Relation_Contact ) ) {
			throw new IsRequired( "Relation_Contact" );
		}
		if ( blank( $relation->Relation_Contact["LastName"] ) ) {
			throw new IsRequired( "Relation_Contact->LastName" );
		}
		if ( blank( $relation->Relation_Contact["FirstName"] ) ) {
			throw new IsRequired( "Relation_Contact->FirstName" );
		}
		$options = $relation->toArray();
		
		return $this->postEndpointRequest( 'addprivaterelation/', $options );
	}
	
	/**
	 * This PUT method allows you to update information for a CRM relation. The relation to be updated is identified by
	 * the ‘RelationGUID’ parameter.
	 *
	 * RelationGUID
	 * Every relation has an unique identifier. Using the ListRelations endpoint you can extract the identifier for each relation.
	 *
	 * @param Relation $relation
	 *
	 * @return array
	 * @throws IsRequired
	 */
	public function updateRelation(Relation $relation){
		if ( blank( $relation->RelationGUID ) ) {
			throw new IsRequired( "RelationGUID" );
		}
		$options = $relation->toArray();
		
		return $this->putEndpointRequest( 'updaterelation/', $options );
	}
}