<?php

namespace DaktaDeo\Silvasoft;
use DaktaDeo\Silvasoft\Exceptions\BadRequest;
use DaktaDeo\Silvasoft\Exceptions\IsRequired;
use Exception;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\RequestOptions;

trait HasOrders {
	/**
	 * From the Silvasoft docs:
	 *
	 * Retrieve a list of sales orders from your administration.
	 * A maximum of 50 orders will be returned at once. Use offset and
	 * limit parameters to retrieve more orders over multiple calls.
	 *
	 * @param array $parameters
	 *
	 * @return Collection of Orders
	 * @throws Exception
	 */
	public function listOrders( Array $parameters ) {
		$arr = $this->getEndpointRequest( 'listorders/', $parameters );
		$lst = Order::hydrate( $arr );
		
		return $lst->flatten();
	}
	
	/**
	 * From the Silvasoft docs:
	 *
	 * This POST method allows you to create a new order with multiple orderlines, associated to a customer.
	 *
	 * Notes:
	 *
	 * Maximum 100 orderlines per order allowed.
	 * If CustomerNumber is used to identify the customer, the number must be larger then zero. A zero value will be interpreted as empty.
	 * If CustomerName is used to identify the customer and you have multiple customers with exactly the same name,
	 * we will use the first found match.
	 *
	 * @param Order $order
	 *
	 * @return array
	 * @throws IsRequired
	 */
	public function addOrder( Order $order ) {
		if ( blank( $order->CustomerNumber )  && blank( $order->CustomerName )) {
				throw new IsRequired( "CustomerName or CustomerNumber" );
		}
		$options = $order->toArray();
		
		return $this->postEndpointRequest( 'addorder/', $options );
	}
	
}